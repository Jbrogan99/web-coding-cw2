# 2.0 How to run the code # 

### 2.1 Opening the file path ###

To run the code, open the terminal and cd into the file where you have saved the program. Another way of opening the terminal to this location is to open your file explorer and go to where the document is saved. Right-click anywhere in the file and go to actions and open terminal here. This will automatically cd you into the correct file path. (This is the way for Linux, other operating systems might have slightly different syntax to opening the terminal this way).

### 2.2 Starting the Application ###

Next, type node vending_machine.js. If you have changed the name of the file, type this name into the terminal after node (e.g. node TheFileName). (As the application is run on the command line, node.js is used to enable the application to work.) This will start the application and a selection of options will be displayed, along with your available balance.

### 2.3 Using the application ###

There will be a prompt below the code for the user to enter a number. The numbers correspond to each product's index. Depending on the choice the user picks and their current available balance, it will either be a successful or unsuccessful transaction. If the purchase/option is unsuccessful, then an alert will guide the user on selecting the correct choice. Lastly, an exit button has been implemented to stop the application from running and let the user leave.

### 2.4 Error Handling ###
If you are getting an error as such, Error: Cannot find module 'readline-sync'. Follow these instructions:

To ensure readline sync works on your computer, open the command line to where the file is saved. then type in: 

npm install --save readline-sync

npm install

readlineSync tries to let your script have a conversation with the user via a console, even when the input/output stream is redirected. 